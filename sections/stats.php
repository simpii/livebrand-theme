

<section class="stats">
	<div class="container">
	    <!-- STATS -->
	    <div class="row">
	        
	        <!-- START COLUMN -->
	        <div class="col-lg-3 col-sm-3">
	            <div class="stat wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="1.5s" data-wow-delay="0.15s" style="visibility: visible;-webkit-animation-duration: 1.5s; -moz-animation-duration: 1.5s; animation-duration: 1.5s;-webkit-animation-delay: 0.15s; -moz-animation-delay: 0.15s; animation-delay: 0.15s;">
	                <div class="icon-top red-text">
	                    <i class="icon-design-graphic-tablet-streamline-tablet"></i>
	                </div>
	                <div class="stat-text">
	                <h3 class="white-text red-border-bottom">1,236</h3>
	                <h6>Projects Completed</h6>
	                </div>
	            </div>
	        </div> <!-- / END COLUMN -->
	        
	        <!-- START COLUMN -->
	        <div class="col-lg-3 col-sm-3">
	            <div class="stat wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="1.75s" data-wow-delay="0.30s" style="visibility: visible;-webkit-animation-duration: 1.75s; -moz-animation-duration: 1.75s; animation-duration: 1.75s;-webkit-animation-delay: 0.30s; -moz-animation-delay: 0.30s; animation-delay: 0.30s;">
	                <div class="icon-top green-text">
	                    <i class="icon-man-people-streamline-user"></i>
	                </div>
	                <div class="stat-text">
	                <h3 class="white-text green-border-bottom">196</h3>
	                <h6>Happy Clients</h6>
	                </div>
	            </div>
	        </div> <!-- / END COLUMN -->
	        
	        <!-- START COLUMN -->
	        <div class="col-lg-3 col-sm-3">
	            <div class="stat wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="2s" data-wow-delay="0.45s" style="visibility: visible;-webkit-animation-duration: 2s; -moz-animation-duration: 2s; animation-duration: 2s;-webkit-animation-delay: 0.45s; -moz-animation-delay: 0.45s; animation-delay: 0.45s;">
	                <div class="icon-top blue-text">
	                    <i class="icon-email-mail-streamline"></i>
	                </div>
	                <div class="stat-text">
	                <h3 class="white-text blue-border-bottom">56,236</h3>
	                <h6>Mail Conversation</h6>
	                </div>
	            </div>
	        </div> <!-- / END COLUMN -->
	        
	        <!-- START COLUMN -->
	        <div class="col-lg-3 col-sm-3">
	            <div class="stat wow fadeInUp animated animated" data-wow-offset="30" data-wow-duration="2.25s" data-wow-delay="1s" style="visibility: visible;-webkit-animation-duration: 2.25s; -moz-animation-duration: 2.25s; animation-duration: 2.25s;-webkit-animation-delay: 1s; -moz-animation-delay: 1s; animation-delay: 1s;">
	                <div class="icon-top yellow-text">
	                    <i class="icon-picture-streamline-1"></i>
	                </div>
	                <div class="stat-text">
	                <h3 class="white-text yellow-border-bottom">2,519</h3>
	                <h6>Photos Taken</h6>
	                </div>
	            </div>
	        </div> <!-- / END COLUMN -->
	    </div>
</div>
</section>