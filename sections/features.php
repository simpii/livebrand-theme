
<section class="features" id="features">
<div class="container">
		
		<!-- SECTION HEADER -->
	<div class="section-header">
		
		<!-- SECTION TITLE -->
		<h2 class="dark-text">Konkurenční výhody</h2>
		
		<!-- SHORT DESCRIPTION ABOUT THE SECTION -->
		<!--h6>
			We design &amp; develop qaulity products to help small &amp; medium level business.
		</h6-->
	</div>
	<!-- / END SECTION HEADER -->
	
	<!-- FEATURS -->
	<div class="row">
		
		<!-- FEATURES COLUMN LEFT -->
		<div class="col-md-6 col-sm-6 wow fadeInLeft animated animated" data-wow-offset="30" data-wow-duration="1.5s" data-wow-delay="0.15s" style="visibility: visible;-webkit-animation-duration: 1.5s; -moz-animation-duration: 1.5s; animation-duration: 1.5s;-webkit-animation-delay: 0.15s; -moz-animation-delay: 0.15s; animation-delay: 0.15s;">
			
			<!-- FEATURE -->
			<div class="feature">
				<div class="feature-icon">
					<i class="icon-heart-1"></i>
				</div>
				<h5>Slušnost v podnikání</h5>
				<p>
				<ul>
				<li>Co řekneme, to dodržíme (žádné sliby a chlácholení)</li>
				<li>Mluvíme na rovinu (žádné chození kolem horké kaše a výmluvy)</li>
				<li>Pracujeme pečlivě (zakládáme si na kvalitně odvedené práci)</li>
				<li>Stíháme termíny (snažíme se odevzdat práci včas)</li>
				</ul>
				</p>
			</div>
			<!-- / END FEATURE -->
			
			<!-- FEATURE -->
			<div class="feature">
				<div class="feature-icon">
					<i class="icon-bulb"></i>
				</div>
				<h5>Online marketingový tým</h5>
				<p>
				<ul>
				<li>Jsme tým freelancerů s projekt managerem</li>
				<li>Jednáte s konkrétními lidmi, kteří pro vás dělají danou práci</li>
				<li>Můžeme nabídnout široké portfolio služeb jako agentura</li>
				<li>Jsme velmi transparentní, otevření s jasnou strukturou fungování</li>
				</ul>				</p>
			</div>
			<!-- / END FEATURE -->
			
			<!-- FEATURE -->
			<div class="feature">
				<div class="feature-icon">
					<i class="icon-settings-1"></i>
				</div>
				<h5>Svoboda v práci, ale skutečná</h5>
				<p>
				<ul>
				<li>Každý pracuje odkud chce</li>
				<li>Rozhodujeme skupinově</li>
				<li>Výběr klienta je na každém jednotlivci</li>
				<li>Nabízíme obrovský prostor pro realizaci jakéhokoliv specialisty v online marketingu</li>
				</ul>				</p>
			</div>
			<!-- / END FEATURE -->
		</div> <!-- / FEATURES COLUMN LEFT -->
		
		<!-- FEATURES COLUMN RIGHT -->
		<div class="col-md-6 col-sm-6 wow fadeInRight animated animated" data-wow-offset="30" data-wow-duration="1.5s" data-wow-delay="0.15s" style="visibility: visible;-webkit-animation-duration: 1.5s; -moz-animation-duration: 1.5s; animation-duration: 1.5s;-webkit-animation-delay: 0.15s; -moz-animation-delay: 0.15s; animation-delay: 0.15s;">
			<!-- FEATURE -->
			<div class="feature">
				<div class="feature-icon">
					<i class="icon-params"></i>
				</div>
				<h5>Revoluční pojetí marketingu</h5>
				<p>
				<ul>
				<li>Snažíme se, aby se klienti mohli živit tím, co je baví</li>
				<li>Marketing je založený na vztazích se zákazníky, nikoli zisku</li>
				<li>Marketing by měl být hravý a kreativní, nevnucovat lidem reklamu</li>
				<li>Klienty si vybíráme většinou sami a pečlivě vážíme náš přínos</li>
				</ul>				</p>
			</div>
			<!-- / END FEATURE -->
			
			<!-- FEATURE -->
			<div class="feature">
				<div class="feature-icon">
					<i class="icon-handle-streamline-vector"></i>
				</div>
				<h5>Dlohodobé vize a partnerství</h5>
				<p>
				<ul>
				<li>V tomto uspěchaném světě nabízíme marketingový plán na rok dopředu</li>
				<li>Plánujeme kampaně, které jsou ziskové z dlouhodobého hlediska</li>
				<li>Budujeme dlouhodobé, pevné a prospěšné vztahy</li>
				<li>Věříme, že profesionálové nekonkurují, ale spolupracují</li>
				</ul>				</p>
			</div>
			<!-- / END FEATURE -->
			
			
			<!-- / END FEATURE -->
			
		</div> <!-- / END FEATURES COLUMN RIGHT -->
	</div> <!-- / END FEATURES -->
</div> <!-- / END CONTAINER -->
</section>